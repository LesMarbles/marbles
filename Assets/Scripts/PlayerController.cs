﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerController : MonoBehaviour {

/*
	public float speed;
	
	void FixedUpdate () 
	{
		//Movement Key
		float xMove = Input.GetAxis ("Horizontal");
		float ZMove = Input.GetAxis ("Vertical");
		
		//Movement By Rolling
		Vector3 movement = new Vector3 (xMove, 0.0f, ZMove);
		GetComponent<Rigidbody> ().AddForce (movement * speed * Time.deltaTime);
	}

*/
//##################################################################################################

	public float speed = 0.1F;
	//Vector3 originPos;
	Vector3 targetPos;
	//private SpringJoint spring;
	GameObject marbles;

	
//	void Start()
//	{
//		spring = GetComponent<SpringJoint> ();
//		rb = GetComponent<Rigidbody> ();
//	}

	void Update()
	{
			/*
			if (Input.GetTouch (0).phase == TouchPhase.Began) 
			{
				Create SpringJoint here.
			}
			*/
		if (Input.touchCount > 0) 
		{
			if (Input.GetTouch (0).phase == TouchPhase.Moved) 
			{
				//originPos = GetComponent<Rigidbody>().transform.position;
				marbles = GameObject.FindWithTag("Marble");
				targetPos = marbles.GetComponent<Rigidbody>().position;
				Vector3 targetDir = targetPos - transform.position;
				GetComponent<Rigidbody>().AddForce(targetDir * speed, ForceMode.Force);

			}



			/*
			if (Input.GetTouch (0).phase == TouchPhase.Moved) 
			{
				// Get movement of the finger since last frame
				Vector3 touchDeltaPosition = Input.GetTouch (0).deltaPosition;
				
				// Move object across XY plane
				transform.Translate (touchDeltaPosition.x * speed, 0, touchDeltaPosition.z * speed);
//				rb.AddForce(transform.forward * speed);

				
			}
			*/
//			if (Input.GetTouch (0).phase == TouchPhase.Ended || Input.GetTouch (0).phase == TouchPhase.Canceled) 
//			{
//				Destroy (spring);
//			}
		}
	}
	










// ##############################################################################################################################
//	const float k_Spring = 50.0f;
//	const float k_Damper = 5.0f;
//	const float k_Drag = 10.0f;
//	const float k_AngularDrag = 5.0f;
//	const float k_Distance = 0.2f;
//	const bool k_AttachToCenterOfMass = false;
//	
//	private SpringJoint m_SpringJoint;
//	
//	private void Update()
//	{
//		// Make sure the user pressed the mouse down
//		//		if (!Input.GetMouseButtonDown(0))
//		if (Input.touchCount <= 0)
//		{
//			return;
//		}
//		
//		var mainCamera = FindCamera();
//		
//		// We need to actually hit an object
//		RaycastHit hit = new RaycastHit();
//		if (
//			!Physics.Raycast(mainCamera.ScreenPointToRay(Input.GetTouch(0).position).origin,
//		                 mainCamera.ScreenPointToRay(Input.GetTouch(0).position).direction, out hit, 100,
//		                 Physics.DefaultRaycastLayers))
//		{
//			return;
//		}
//		// We need to hit a rigidbody that is not kinematic
//		if (!hit.rigidbody || hit.rigidbody.isKinematic)
//		{
//			return;
//		}
//		
//		if (!m_SpringJoint)
//		{
//			var go = new GameObject("Rigidbody dragger");
//			Rigidbody body = go.AddComponent<Rigidbody>();
//			m_SpringJoint = go.AddComponent<SpringJoint>();
//			body.isKinematic = true;
//		}
//		
//		m_SpringJoint.transform.position = hit.point;
//		m_SpringJoint.anchor = Vector3.zero;
//		
//		m_SpringJoint.spring = k_Spring;
//		m_SpringJoint.damper = k_Damper;
//		m_SpringJoint.maxDistance = k_Distance;
//		m_SpringJoint.connectedBody = hit.rigidbody;
//		
//		StartCoroutine("DragObject", hit.distance);		
//		
//	}
//	
//	
//	private IEnumerator DragObject(float distance)
//	{
//		var oldDrag = m_SpringJoint.connectedBody.drag;
//		var oldAngularDrag = m_SpringJoint.connectedBody.angularDrag;
//		m_SpringJoint.connectedBody.drag = k_Drag;
//		m_SpringJoint.connectedBody.angularDrag = k_AngularDrag;
//		var mainCamera = FindCamera();
//		//		while (Input.GetMouseButton(0))
//		while(Input.touchCount > 0)
//		{
//			var ray = mainCamera.ScreenPointToRay(Input.mousePosition);
//			m_SpringJoint.transform.position = ray.GetPoint(distance);
//			yield return null;
//		}
//		if (m_SpringJoint.connectedBody)
//		{
//			m_SpringJoint.connectedBody.drag = oldDrag;
//			m_SpringJoint.connectedBody.angularDrag = oldAngularDrag;
//			m_SpringJoint.connectedBody = null;
//			//			if(spring.connectedBody)
//			//			{
//			//				spring.connectedBody = null;
//			//			}
//		}
//	}
//	
//	
//	private Camera FindCamera()
//	{
//		if (GetComponent<Camera>())
//		{
//			return GetComponent<Camera>();
//		}
//		
//		return Camera.main;
//	}
//	
	public void restartGame()
	{
		Application.LoadLevel (Application.loadedLevel);
	}
}
